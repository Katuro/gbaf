<?php
  require 'shared/autoload.php';

  $auth = App::getAuth();
  $db = App::getDatabase();
  $auth->restrict();
  $user_id = $_SESSION['auth']->id;
  
  if (!empty($_POST))
  {
    $validator = new Validator($_POST);

    if (!empty($_POST['password'])){
      $validator->isSecured('password');
      $validator->isConfirmed('password', "Le mot de passe et le mot de passe confirmé ne sont pas identique.");
      if ($validator->isValid()){
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        $db->query('UPDATE user SET password = ? WHERE id = ?', [$password, $user_id]);
        $session->setFlash('success', "Mot de passe modifié avec succès.");
      }
    }

    $user = new User;
    if (!empty($_POST['username']) && $validator->isValid())
      $user->updateUsername($validator, $db, $user_id);

    if (!$validator->isValid()) {
      $session = Session::getInstance();
      $session->setFlash('danger', array_values($validator->getErrors())[0]);
    }
    else
      $auth->connect($user->getUserFromId($db, $user_id));

  }
  require 'shared/header.php';
?>

<div class="gbaf-row-padding gbaf-padding-32 gbaf-container">
  <div class="gbaf-content">
    <h2><?= $_SESSION['auth']->name.' '.$_SESSION['auth']->surname ?></h2>

    <h5>Changer de mot de passe :</h5>
    <form action="" method="post">
      <div class="form-group">
        <input type="password" class="form-control" name="password" placeholder="Mot de passe"/>
      </div>
      <div class="form-group">
        <input type="password" class="form-control" name="password_confirm" placeholder="Confirmation du mot de passe."/>
      </div>
      <button class="gbaf-button gbaf-black gbaf-padding-large gbaf-large gbaf-margin-top">Mettre à jour</button>
    </form>
    <br/>
    <h5>Modifie ton surnom :</h5>
    <form action="" method="post">
      <div class="form-group">
        <input type="text" class="form-control" name="username" placeholder="<?= $_SESSION['auth']->username ?>"/>
      </div>

      <button class="gbaf-button gbaf-black gbaf-padding-large gbaf-large gbaf-margin-top">Sauvegarder</button>
    </form>
  </div>
</div>

<?php require 'shared/footer.php'; ?>