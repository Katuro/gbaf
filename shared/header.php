<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>GBAF</title>
  <link rel="icon" href="../images/favicon.ico">
  <link href="../css/general.css" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script language="javascript" src="javascript/application.js"></script>
</head>

<body>
  <!-- Navbar -->
  <div class="gbaf-top">
    <div class="gbaf-bar gbaf-red gbaf-card gbaf-left-align gbaf-large">
      <a class="gbaf-bar-item gbaf-button gbaf-hide-medium gbaf-hide-large gbaf-right gbaf-padding-large gbaf-hover-white gbaf-large gbaf-red" href="javascript:void(0);" onclick="myFunction()" title="Ouvrir le menu"><i class="fa fa-bars"></i></a>
      <?php if(isset($_SESSION['auth'])): ?>
        <img src="../images/favicon.ico" height='4em'>
        <a href="home.php" class="gbaf-bar-item gbaf-button gbaf-white"><img src="../images/favicon.ico" height='35px'></a>
        <a href="account.php" class="gbaf-bar-item gbaf-button gbaf-hide-small gbaf-padding-large gbaf-right gbaf-hover-white"><i class="fa fa-user"></i> <?= $_SESSION['auth']->name.' '.$_SESSION['auth']->surname; ?></a>
        <a href="actions/logout.php" class="gbaf-bar-item gbaf-button gbaf-hide-small gbaf-padding-large gbaf-right gbaf-hover-white">Se déconnecter</a>
      <?php else: ?>
        <a href="register.php" class="gbaf-bar-item gbaf-button gbaf-hide-small gbaf-padding-large gbaf-hover-white">S'inscrire</a>
        <a href="login.php" class="gbaf-bar-item gbaf-button gbaf-hide-small gbaf-padding-large gbaf-hover-white">Se connecter</a>
      <?php endif; ?>
    </div>

    <!-- Navbar on small screens -->
    <div id="navBar" class="gbaf-bar-block gbaf-white gbaf-hide gbaf-hide-large gbaf-hide-medium gbaf-large">
      <?php if(isset($_SESSION['auth'])): ?>
        <a href="account.php" class="gbaf-bar-item gbaf-button gbaf-padding-large"><?= $_SESSION['auth']->name.' '.$_SESSION['auth']->surname; ?></a>
        <a href="actions/logout.php" class="gbaf-bar-item gbaf-button gbaf-padding-large">Se déconnecter</a>
      <?php else: ?>
        <a href="register.php" class="gbaf-bar-item gbaf-button gbaf-padding-large">S'inscrire</a>
        <a href="login.php" class="gbaf-bar-item gbaf-button gbaf-padding-large">Se connecter</a>
      <?php endif; ?>
    </div>
  </div>

  <div class="gbaf-margin-navbar"></div>

    <?php if(Session::getInstance()->hasFlashes()): ?>
      <?php foreach(Session::getInstance()->getFlashes() as $type => $message): ?>
        <div class="gbaf-padding-large gbaf-black gbaf-<?= $type; ?>">
          <?= $message; ?>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>


