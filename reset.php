<?php
  require 'shared/autoload.php';

  if (isset($_GET['reset_token']) && isset($_GET['id']))
  {
    $auth = App::getAuth();
    $db = App::getDatabase();
    $user = User::getUserFromResetToken($db, $_GET['id'], $_GET['reset_token']);
    if ($user)
    {
      if (!empty($_POST))
      {
        $validator = new Validator($_POST);
        $validator->isSecured('password');
        $validator->isConfirmed('password', 'Le mot de passe et le mot de passe confirmé ne sont pas identique.');
        if ($validator->isValid()){
          $password = User::hashPassword($_POST['password']);
          $db->query('UPDATE user SET password = ?, reset_at = NULL, reset_token = NULL WHERE id = ?', [$password, $user->id]);
          Session::getInstance()->setFlash('success', 'Your password have been reset.');
          $auth->connect($user);
          App::redirect('login.php');
        }
        else
          Session::setFlash('danger', array_values($validator->getErrors())[0]);
      }
    }
    else
    {
      Session::getInstance()->setFlash('danger', 'Invalid token.');
      App::redirect('login.php');
    }
  }
  else
     App::redirect('login.php');
?>

<?php require_once 'shared/header.php'; ?>

<h2>Reset password.</h1>

<form action="" method="POST">

    <div class="form-group">
        <label for="">Password</label>
        <input type="password" name="password" class="form-control"/>
    </div>

    <div class="form-group">
        <label for="">Password confirmation</label>
        <input type="password" name="password_confirm" class="form-control"/>
    </div>

    <button type="submit" class="btn btn-primary">Reset password</button>

</form>

<?php require 'shared/footer.php'; ?>