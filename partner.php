<?php
  require 'shared/autoload.php';

  $auth = App::getAuth();
  $db = App::getDatabase();
  $auth->restrict();
  $user_id = $_SESSION['auth']->id;

  $partner = new Partner;
  $neverComment = $partner->neverComment($db, $user_id, $_GET["id"]);
  $partner = $partner->getPartnerFromId($db, $_GET["id"]);
  if (empty($partner)){
    Session::setFlash('danger', "Ce partenaire n'existe pas.");
    App::redirect('index.php');
  }

  $comments = new Comment;
  $comments = $comments->getPartnerComments($db, $partner->id);
  $user = new User;
  $comments_nb = $db->query('SELECT COUNT(*) FROM comment WHERE partner_id = ?', ["$partner->id"])->fetchColumn();
  $vote = new Vote;
  $positives_votes_nb = $vote->countPositivesVotes($db, $partner->id);
  $negatives_votes_nb = $vote->countNegativesVotes($db, $partner->id);

  require 'shared/header.php';
?>

<div class="gbaf-row-padding gbaf-padding-64 gbaf-container">
  <div class="gbaf-content gbaf-row-padding">
    <img src=<?= $partner->logo ?> class='gbaf-image gbaf-border img-center'>
    <h2><?= $partner->name ?></h2>
    <p class="gbaf-padding-16"><?= $partner->description ?></p>

    <div class="gbaf-col m12">
      <h2>
        <?= $comments_nb ?> Commentaires
        <div class="gbaf-right">
          <?= $positives_votes_nb ?><a href="actions/vote.php?partner_id=<?= $partner->id; ?>&value=1"><i class="fa fa-thumbs-up gbaf-text-green"></i></a>
          <?= $negatives_votes_nb ?><a href="actions/vote.php?partner_id=<?= $partner->id; ?>&value=0"><i class="fa fa-flip-horizontal fa-thumbs-down gbaf-text-red"></i></a>
        </div>
      </h2>
    </div>

    <?php foreach($comments as $comment): ?>
      <?php $date = new DateTime($comment->created_at); ?>
      <div class="gbaf-col m12">
        <div class="gbaf-card gbaf-round gbaf-white">
          <div class="gbaf-container gbaf-padding">

            <div class="w3-col m10 w3-container">
              <h4><?= $user->getUserFromId($db, $comment->user_id)->username; ?> <span class="w3-opacity w3-medium" style="font-size: 0.5em;"><?= $date->format('d/m/Y') ?></span></h4>
              <p><?= $comment->content ?></p>
            </div>

          </div>
        </div>
        </br>
      </div>
    <?php endforeach; ?>

    <?php if(!$neverComment): ?>
      <p class="gbaf-text-red gbaf-center">Vous avez déjà laissé un commentaire.</p>
    <?php else: ?>
      <div class="gbaf-col m12">
        <div class="gbaf-card gbaf-round gbaf-white">
          <div class="gbaf-container gbaf-padding">
            <h6 class="gbaf-opacity">Laisse un commentaire :</h6>
            <form method="POST" action="actions/submit_comment.php">
              <textarea id='comment-form' name="comment" class="gbaf-border gbaf-padding gbaf-input" rows="3"></textarea>
              <input type="hidden" name="partner_id" value="<?= $partner->id ?>" />
              </br>
              <input type="submit" name="submit" value="Commenter" class="gbaf-button gbaf-red gbaf-theme" />
            </form>
          </div>
        </div>
      </div>
    <?php endif; ?>

  </p>
</div>

<?php require 'shared/footer.php'; ?>
