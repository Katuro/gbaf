<?php
  require 'shared/autoload.php';

  if (!empty($_POST)){
    $db = App::getDatabase();
    $user = User::getUserFromUsername($db, $_POST['username']);

    if ($user && $user->question == $_POST['question'] && $user->answer == $_POST['answer']){
      $new_password = Str::random(14).'a2';
      Session::getInstance()->setFlash('success', "Votre demande a bien été prise en compte. Votre nouveau mot de passe est : $new_password");

      User::updatePassword($db, $new_password, $user->id);
      App::redirect('login.php');
    }else{
      Session::getInstance()->setFlash('danger', 'Votre pseudonyme, votre réponse ou votre question est incorrect.');
    }
  }
?>

<?php require_once 'shared/header.php'; ?>

<div class="gbaf-row-padding gbaf-padding-64 gbaf-container">
  <div class="gbaf-half gbaf-content gbaf-row-padding">
    <h2>Mot de passe oublié</h2>

    <form action="" method="POST">

      <div class="gbaf-section">
        <label for="">Pseudonyme</label>
        <input type="text" name="username" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Question</label>
        <select name="question" class="gbaf-input gbaf-round gbaf-text-large gbaf-white">
          <?php foreach(User::QuestionsList() as $key=>$value): ?>
            <option value="<?=$key?>"><?= $value ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="gbaf-section">
        <label for="">Réponse</label>
        <input type="text" name="answer" class="gbaf-input gbaf-round" required/>
      </div>

      <button type="submit" class="gbaf-button gbaf-red gbaf-padding-large gbaf-text-large gbaf-right">Envoyer</button>
    </form>

  </div>
</div>
<?php require 'shared/footer.php'; ?>