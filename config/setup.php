<?php
    require 'database.php';

    try {
        $mysql_db = new PDO("mysql:dbname=mysql;host=$DB_HOST;charset=utf8", $DB_USER, $DB_PASSWORD);
        $mysql_db->exec("CREATE DATABASE IF NOT EXISTS `phpDB`");

        $db = new PDO("mysql:dbname=$DB_NAME;host=$DB_HOST;charset=utf8", $DB_USER, $DB_PASSWORD);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $query = file_get_contents("../config/phpDB.sql");
        $sql = $db->prepare($query);
        $sql->execute();
        echo "Database created successfully\n";
    }
    catch(PDOException $e){
        echo $sql . "\n" . $e->getMessage();
    }

$db = null;
?>