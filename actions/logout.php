<?php
  require '../shared/autoload.php';
  $auth = App::getAuth();
  $db = App::getDatabase();
	$auth->restrict();

  App::getAuth()->logout();
  Session::getInstance()->setFlash('success', 'Vous êtes déconnecté.');
  App::redirect('../login.php');
