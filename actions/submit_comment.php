<?php
  require '../shared/autoload.php';

  $auth = App::getAuth();
  $db = App::getDatabase();
  $auth->restrict();

  $content = htmlspecialchars($_POST['comment']);
  $partner_id = $_POST["partner_id"];
  $user = $auth->current_user();

  if (!Comment::neverComment()){
    $db->query("INSERT INTO comment SET user_id = ?, partner_id = ?, content = ?", [
    $user->id,
    $partner_id,
    $content
    ]);
  } else {
    $session->setFlash('danger', "Vous avez déjà commenté.");
    App::redirect($_SERVER['HTTP_REFERER']);
  }


  App::redirect("../partner.php?id=$partner_id");
?>