<?php
  require_once '../shared/autoload.php';
  $auth = App::getAuth();
  $db = App::getDatabase();
  $auth->restrict();
  $session = Session::getInstance();

  $sender_id = $auth->current_user()->id;
  $partner_id = $_GET['partner_id'];
  $value = $_GET['partner_id'] == 0 ? false : true;
  $vote = new Vote;

  if ($vote->create($db, $sender_id, $partner_id, $value))
    $session->setFlash('success', "Votre avis a bien été pris en compte.");
  else
    $session->setFlash('danger', "Vous avez déjà donné votre avis.");
    App::redirect($_SERVER['HTTP_REFERER']);
?>