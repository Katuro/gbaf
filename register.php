<?php
    require 'shared/autoload.php';

    if (!empty($_POST)){

        $db = App::getDatabase();
        $validator = new Validator ($_POST);

        $validator->isAlpha('username', "Invalid username.");
        if ($validator->isValid())
            $validator->isUniq($db, 'username', 'user', "Ce pseudonyme est déjà pris.");

        $validator->isSecured('password');
        $validator->isConfirmed('password', "Le mot de passe et le mot de passe confirmé ne sont pas identique.");

        if ($validator->isValid()){
            User::register(App::getAuth(), $db, $_POST['name'], $_POST['surname'], $_POST['username'], $_POST['password'], $_POST['question'], $_POST['answer']);

            Session::getInstance()->setFlash('success', 'Votre compte a bien été créé.');
            App::redirect('login.php');
        }else{
            $errors = $validator->getErrors();
        }
    }
?>
<?php require 'shared/header.php'; ?>

<?php if(!empty($errors)): ?>

<div class="alert alert-danger">
    <p>Veuillez modifier les erreurs suivantes : </p>
    <ul>
        <?php foreach($errors as $error): ?>
            <li><?= $error; ?></li>
        <?php endforeach; ?>
    </ul>
</div>

<?php endif; ?>

<div class="gbaf-row-padding gbaf-padding-64 gbaf-container">
  <div class="gbaf-half gbaf-content gbaf-row-padding">
    <h2>S'inscrire</h2>

    <form action="" method="POST">

      <div class="gbaf-section">
        <label class="">Prénom</label>
        <input type="text" name="name" class="gbaf-input gbaf-round gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Nom de famille</label>
        <input type="text" name="surname" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Pseudonyme</label>
        <input type="text" name="username" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Mot de passe</label>
        <input type="password" name="password" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Confirmer le mot de passe</label>
        <input type="password" name="password_confirm" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
        <label for="">Question</label>
        <select name="question" class="gbaf-input gbaf-round gbaf-text-large gbaf-white">
          <?php foreach(User::QuestionsList() as $key=>$value): ?>
            <option value="<?=$key?>"><?= $value ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="gbaf-section">
        <label for="">Réponse à la question secrète</label>
        <input type="text" name="answer" class="gbaf-input gbaf-round" required/>
      </div>

      <button type="submit" class="gbaf-button gbaf-red gbaf-padding-large gbaf-text-large gbaf-right">S'inscrire</button>
    </form>
  </div>
</div>

<?php require 'shared/footer.php'; ?>