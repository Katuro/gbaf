<?php
  require 'shared/autoload.php';

  $auth = App::getAuth();
  $db = App::getDatabase();
  echo $db->lastInsertId();

  if ($auth->current_user())
    App::redirect('account.php');

  if(!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])){
    $user = $auth->login($db, $_POST['username'], $_POST['password']);
    $session = Session::getInstance();
    if ($user){
      $session->setFlash('success', "Bonjour $user->username !");
      App::redirect('account.php');
    } else {
        $session->setFlash('danger', "Pseudonyme ou mot de passe incorrect.");
    }
  }

  require 'shared/header.php';
?>

<div class="gbaf-row-padding gbaf-padding-64 gbaf-container">
  <div class="gbaf-half gbaf-content gbaf-row-padding">
    <h2>Se connecter</h2>

    <form action="" method="POST">

      <div class="gbaf-section">
        <label for="">Pseudonyme</label>
        <input type="text" name="username" class="gbaf-input gbaf-round" required/>
      </div>

      <div class="gbaf-section">
          <label for="">Mot de passe <a href="forget.php">(mot de passe oublié ?)</a></label>
          <input type="password" name="password" class="gbaf-input gbaf-round" required/>
      </div>

      <button type="submit" class="gbaf-button gbaf-red gbaf-padding-large gbaf-text-large gbaf-right">Connexion</button>
    </form>
  </div>
</div>

<?php require 'shared/footer.php'; ?>