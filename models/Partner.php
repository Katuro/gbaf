<?php
  class Partner{
    
    public function getAll($db){
        return $db->query("SELECT * FROM partner");
    }

    public function getPartnerFromId($db, $partner_id){
      $partner = $db->query('SELECT * FROM partner WHERE id = ?', [$partner_id])->fetch();
      if (empty($partner))
        return false;
      else
        return $partner;
    }

    public function neverComment($db, $user_id, $partner_id){
      $comment = $db->query('SELECT user_id FROM comment WHERE user_id = ?  AND partner_id = ? LIMIT 1', [$user_id, $partner_id])->fetch();
      return empty($comment);
    }

  }
?>