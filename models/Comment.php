<?php
  class Comment{
    
    public function getPartnerComments($db, $partner_id){
        return $db->query("SELECT * FROM comment WHERE partner_id = ? ORDER BY id", [$partner_id]);
    }

  }