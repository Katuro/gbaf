<?php

  class App{

    static $db = null;

    static function getDatabase(){
      if (!self::$db)
        self::$db = new Database('sora', 'root', 'phpDB', '192.168.99.102');
      return self::$db;
    }

    static function getAuth(){
      return new Auth(Session::getInstance());
    }

    static function redirect($location){
      header("Location: $location");
      exit();
    }
  }