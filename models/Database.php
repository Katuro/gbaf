<?php
    class Database{

        private $_db;

        public function __construct($login, $password, $database_name, $host = '192.168.99.102'){
            $this->_db = new PDO("mysql:dbname=$database_name;host=$host;charset=utf8", $login, $password);
            $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->_db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }

        public function query($query, $params = false){
            if ($params){
                $req = $this->_db->prepare($query);
                $req->execute($params);
            }else{
                $req = $this->_db->query($query);
            }
            return $req;
        }

        public function lastInsertId(){
            $this->_db->lastInsertId();
        }
    }