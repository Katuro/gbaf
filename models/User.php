<?php
  class User{

    public static function QuestionsList() {
      return array("1"=>"Quel est le nom de jeune fille de votre mère ?", "2"=>"Quelle fut votre premiere voiture ?", "3"=>"Quelle est votre couleur préférée ?", "4"=>"Quel est le nom de votre premier animal de compagnie ?", "5"=>"Quel est votre numéro de carte bancaire ?");
    }

    public function getQuestion($selector) {
      return array_search($selector, $this->QuestionsList());
    }


    public static function hashPassword($password){
      return password_hash($password, PASSWORD_BCRYPT);
    }

    public function getUserFromId($db, $user_id){
      $user = $db->query('SELECT * FROM user WHERE id = ?', [$user_id])->fetch();
      if (empty($user))
        return false;
      else
        return $user;
    }

    public static function getUserFromUsername($db, $username){
      $user = $db->query('SELECT * FROM user WHERE username = ?', [$username])->fetch();
      if (empty($user))
        return false;
      else
        return $user;
    }

    public static function register($auth, $db, $name, $surname, $username, $password, $question, $answer){
      $password = User::hashPassword($password);
      $token = Str::random(60);

      $db->query("INSERT INTO user SET name = ?, surname = ?, username = ?, password = ?, question = ?, answer = ?", [
        $name,
        $surname,
        $username,
        $password,
        $question, 
        $answer
        ]);
      $user_id = $db->lastInsertId();
    }

    public function updateUsername($validator, $db, $user_id){
      $validator->isAlpha('username', 'Pseudonyme invalide.');
      if ($validator->isValid())
        $validator->isUniq($db, 'username', 'user', 'Ce pseudonyme est déjà pris.');
      if ($validator->isValid()){
        $db->query('UPDATE user SET username = ? WHERE id = ?', [$_POST['username'], $user_id]);
        $_SESSION['flash']['success'] = 'Pseudonyme mis à jour.';
      }
    }

    public static function updatePassword($db, $new_password, $user_id){
      $password = User::hashPassword($new_password);
      $db->query('UPDATE user SET password = ? WHERE id = ?', [$password, $user_id]);
    }

  }
