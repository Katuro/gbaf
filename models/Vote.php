<?php
  class Vote {

    public function create($db, $user_id, $partner_id, $value){
      if (Vote::haveNeverVoted($db, $user_id, $partner_id)){
        $db->query("INSERT INTO vote SET user_id = ?, partner_id = ?, value = ?", [
          $user_id,
          $partner_id,
          $value
        ]);
        return true;
      }
      else
        return false;
    }

    public function haveNeverVoted($db, $user_id, $partner_id){
      $vote = $db->query('SELECT user_id FROM vote WHERE user_id = ?  AND partner_id = ? LIMIT 1', [$user_id, $partner_id])->fetch();
      return empty($vote);
    }

    public function countPositivesVotes($db, $partner_id) {
      return $db->query('SELECT COUNT(*) FROM vote WHERE value = true AND partner_id = ?', ["$partner_id"])->fetchColumn();
    }

    public function countNegativesVotes($db, $partner_id) {
      return $db->query('SELECT COUNT(*) FROM vote WHERE value = false AND partner_id = ?', ["$partner_id"])->fetchColumn();
    }

  }