<?php
  class Auth{

    private $session;

    public function __construct($session){
      $this->session = $session;
    }

    public function confirm($db, $user_id, $token){

      $user = $db->query('SELECT * FROM user WHERE id = ?', [$user_id])->fetch();

      if ($user && $user->confirmation_token == $token){
            $req = $db->query('UPDATE user SET confirmation_token = NULL, confirmed_at = NOW() WHERE id = ?', [$user_id]);
            $this->session->write('auth', $user);
            return true;
      }else{
        return false;
      }
    }

    public function current_user(){
      if (!$this->session->read('auth')){
        return false;
      }
      return $this->session->read('auth');
    }

    public function restrict(){
      if (!$this->session->read('auth')){
        $this->session->setFlash('danger', 'Accès refusé.');
        header('Location: /login.php');
        exit();
      }
    }

    public function connect($user){
      $this->session->write('auth', $user);
    }

    public function login($db, $username, $password){
        $user = $db->query('SELECT * FROM user WHERE (username = :username)', ['username' => $username])->fetch();
        if (password_verify($password, $user->password))
        {
          $this->connect($user);
          return $user;
        }
        else
          return false;
    }

    public function logout(){
      Session::delete('auth');
    }

    // public function resetPassword($db, $email){
    //   $user = $db->query('SELECT * FROM users WHERE email = ? AND confirmed_at IS NOT NULL', [$email])->fetch();
    //   if ($user)
    //   {
    //     $reset_token = Str::random(60);
    //     $db->query('UPDATE users SET reset_token = ?, reset_at = NOW() WHERE id = ?', [$reset_token, $user->id]);
    //     mail($_POST['email'], 'Reset your password.', "Use this link in order to reset your password.\n\nhttp://localhost:1234/reset.php?id={$user->id}&reset_token=$reset_token");
    //     return $user;
    //   }
    //   return false;
    // }
}