<?php
    class Validator{

        private $data;
        private $errors = [];

        public function __construct($data){
            $this->data = $data;
        }

        private function getField($field){
            if (!isset($this->data[$field])){
                return NULL;
            }
            return $this->data[$field];
        }

        public function isAlpha($field, $errorMsg){
            if (!preg_match('/^[a-zA-Z0-9_]+$/', $this->getField($field))){
                $this->errors[$field] = $errorMsg;
            }
        }

        public function isUniq($db, $field, $table, $errorMsg){
            $record = $db->query("SELECT id FROM $table WHERE $field = ?", [$this->getField($field)])->fetch();
            if ($record){
                $this->errors[$field] = $errorMsg;
            }
        }

        public function isConfirmed($field, $errorMsg = "Le mot de passe et le mot de passe confirmé ne sont pas identique."){
            $value = $this->getField($field);
            if (empty($value) || $value != $this->getField($field . '_confirm')){
                $this->errors[$field] = $errorMsg;
            }
        }

        public function isSecured($field, $errorMsg = ''){
            $value = $this->getField($field);

            if (strlen($value) < 8) {
                $this->errors[$field] = "Password too short!";
            }
            else if (!preg_match("/[0-9]/", $value)) {
                $this->errors[$field] = "Password must include at least one number!";
            } 
            else if (!preg_match("/[a-z]/i", $value)) {
                $this->errors[$field] = "Password must include at least one letter!";
            }
        }

        public function isValid(){
            return empty($this->errors);
        }

        public function getErrors(){
            return $this->errors;
        }

    }