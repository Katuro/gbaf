<?php
  require 'shared/autoload.php';

  $auth = App::getAuth();
  $db = App::getDatabase();
  $auth->restrict();

  $partners = new Partner;
  $partners = $partners->getAll($db);
  require 'shared/header.php';
?>
<div class="bgimg-1 gbaf-display-container gbaf-opacity-min">
  <div class="gbaf-display-middle gbaf-center gbaf-red gbaf-padding-large">
    <span class="gbaf-center gbaf-xlarge gbaf-wide gbaf-animate-opacity">Gouvernement Banque Assurance Français</span>
  </div>
</div>


<div class="gbaf-row-padding gbaf-padding-64 gbaf-container">
  <div class="gbaf-content">
    <div class="gbaf-twothird gbaf-left">
      <h2>Présentation du Gouvernement Banque Assurance Français</h2>
      <p class="gbaf-padding-16">Le Groupement Banque Assurance Français (GBAF) est une fédération représentant les 6 grands groupes français :</p>
      <ul>
        <li>BNP Paribas ;</li>
        <li>BPCE ;</li>
        <li>Crédit Agricole ;</li>
        <li>Crédit Mutuel-CIC ;</li>
        <li>Société Générale ;</li>
        <li>La Banque Postale.</li>
      </ul>

      <p class="gbaf-text-grey">Même s’il existe une forte concurrence entre ces entités, elles vont toutes travailler de 
        la même façon pour gérer près de 80 millions de comptes sur le territoire national.
      </p>
      <p>
        Le GBAF est le représentant de la profession bancaire et des assureurs sur tous les axes de la réglementation financière
         française. Sa mission est de promouvoir l'activité bancaire à l’échelle nationale. C’est aussi un interlocuteur 
         privilégié des pouvoirs publics.
      <p/>
    </div>
  </div>
</div>

<div class="bgimg-2 gbaf-display-container gbaf-opacity-min">
  <div class="gbaf-display-middle gbaf-center gbaf-red gbaf-padding-large">
    <span class="gbaf-center gbaf-xlarge gbaf-wide gbaf-animate-opacity">Les Acteurs et Partenaires</span>
  </div>
</div>

<div class="gbaf-row-padding gbaf-padding-top-64 gbaf-container">
  <div class="gbaf-content">
    <div class="gbaf-twothird gbaf-left">
      <h2>Liste de nos partenaires</h2>
      <p>Liste répertoriant un grand nombre d’informations sur les partenaires et acteurs du groupe 
        ainsi que sur les produits et services bancaires et financiers.</p>
    </div>
  </div>
</div>

<div class="gbaf-padding-32">
  <!-- Partner Container -->
  <?php foreach($partners as $partner): ?>
    <div class="gbaf-container gbaf-content gbaf-card gbaf-round gbaf-padding">
      <div class="gbaf-row">
        <!-- Partner Image -->
        <div class="gbaf-col m3 gbaf-left gbaf-center gbaf-row-padding">
          <img src=<?= $partner->logo ?> class='gbaf-image gbaf-border gbaf-padding-64 gbaf-margin-right'>
        </div>
        <!-- Partner Description -->
        <div class="gbaf-col m7 gbaf-left">
          <h5 class="gbaf-padding-16"><span class="gbaf-tag gbaf-wide"><?= $partner->name; ?></span></h5>

          <p class='gbaf-justify'><?= substr(strip_tags($partner->description), 0, 200) ?>...
          </p>
        </div>
        <!-- Partner Button -->
        <div class="gbaf-col m1 gbaf-left gbaf-center gbaf-row-padding">
          <div class="gbaf-col m3 gbaf-left">
            <p><a href="/partner.php?id=<?= $partner->id; ?>" class="gbaf-leftbar gbaf-button gbaf-padding gbaf-red gbaf-border-black">Afficher la suite »</a></p>
          </div>
        </div>
      </div>
    </div>
    </br>
  <?php endforeach; ?>
</div>


<?php require 'shared/footer.php'; ?>
