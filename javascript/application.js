// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navBar");
  if (x.className.indexOf("gbaf-show") == -1) {
    x.className += " gbaf-show";
  } else {
    x.className = x.className.replace(" gbaf-show", "");
  }
}